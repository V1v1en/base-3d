<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;700&display=swap" rel="stylesheet">
    <title>Document</title>
</head>

<body>
    <header>
        <h1 class="text-logo">
            <i class="fas fa-cubes"></i> 3D base ADMIN <i class="fas fa-cubes"></i>
        </h1>
    </header>

    <div class="container admin">

        <div class="row sheet">
            <div class="col col-md-12 title">
                <h1> # liste d'objets <a href="insert.php" class="btn btn-dark text-center"><i class="far fa-plus-square"></i> Ajouter object</a></h1>

            </div>
            <table class="table table-striped table bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Category</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php


                    // Recuperation de la fonction statique(::) connect dans la CLASSE Database de la page database

                    require 'database.php';
                    $db = Database::connect();
                    

                    // requette SQL

                    $statement = $db->query('SELECT objects.id, objects.name, objects.description, categories.name AS category 
                    FROM objects LEFT JOIN categories ON objects.category = categories.id ORDER BY objects.id DESC');

                    // Affichage

                    // recuperation ligne par ligne (fetch) avec une boucle tant que

                    while($item = $statement->fetch()){
                        printf('<tr>');
                        printf('<td>' . $item['name'] . '</td>');
                        printf('<td>' . $item['description'] . '</td>');                        
                        printf('<td>' . $item['category'] . '</td>');
                        printf('<td width=300>');
                        printf('<a class="btn btn-dark" href="view.php?id=' . $item['id'] . '"><i class="far fa-eye"></i></a>');
                        printf('<a class="btn btn-dark" href="update.php?id=' . $item['id'] . '"><i class="fas fa-hammer"></i></a>');
                        printf('<a class="btn btn-dark" href="delete.php?id=' . $item['id'] . '"><i class="far fa-trash-alt"></i></a>');
                        printf('</td>');
                        printf('</tr>');
                        
                    }
                    Database::disconnect();

                    ?>
                </tbody>

            </table>

        </div>

    </div>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js" integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf" crossorigin="anonymous"></script>
</body>