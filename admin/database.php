<?php 

// Creation d'un objet Database Class Database (POO)


class Database {

// Variables de connection

private static $dbHost = "localhost";
private static $dbName = "base3d";
private static $dbUser = "root";
private static $dBuserPassword = "";
private static $charset = "utf8";

private static $connection = null;
    
public static function connect(){
    
    
        try {

            self::$connection = new PDO("mysql:host=" . self::$dbHost . ";dbname=" . self::$dbName . ";charset=" . self::$charset,self::$dbUser,self::$dBuserPassword);


        }
        catch (PDOException $e){

            die($e->getMessage());
            
        }
        return self::$connection;

}

public static function disconnect(){

    self::$connection = null;

}
}

Database::connect();

// fonction publique utilisable à l'exterieur





?>

