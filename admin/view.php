<?php

// On appel la base de donnée

require 'database.php';


if (!empty($_GET['id'])) {

    $id = verifInput($_GET['id']);
}

// Connexion a la database et on stock la connection dans $db

$db = Database::connect();

$statement = $db->prepare('SELECT objects.id, objects.name, objects.description, objects.image, categories.name AS category 
FROM objects LEFT JOIN categories ON objects.category = categories.id WHERE objects.id = ?');

$statement->execute(array($id));
$item = $statement->fetch();

Database::disconnect();

// Création d'une fonction permettant de securiser les données entrées dans les inputs par l'utilisateur

function verifInput($verif)
{
    $verif = trim($verif);                  //supprime les espaces
    $verif = stripslashes($verif);          //supprime les antislashes
    $verif = htmlspecialchars($verif);      //supprime les charactères spéciaux

    return $verif;
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;700&display=swap" rel="stylesheet">
    <title>Document</title>
</head>

<body>
    <header>
        <h1 class="text-logo">
            <i class="fas fa-cubes"></i> 3D base <i class="fas fa-cubes"></i>
        </h1>
    </header>
    <div class="container view">


        <div class="row">




            <div class="col-sm-6 description" >

                <form>
                    <div class="form-group">
                        <label>Nom :</label><?php echo ' ' . $item['name']; ?>
                    </div>
                    <div class="form-group">
                        <label>Description :</label><?php printf(' ' . $item['description']); ?>
                    </div>
                    <div class="form-group">
                        <label>Categorie :</label><?php printf(' ' . $item['category']); ?>
                    </div>
                </form>
                <div>
                    <a class="btn btn-dark" href="index.php"><i class="fas fa-arrow-left"></i></a>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="card" style="width: 90%;">
                    <img class="card-img-top" src="../<?php printf($item['image']);?>" alt="Card image cap">
                    
                    <div class="card-body">
                        <h5 class="card-title"><?php printf(' ' . $item['name']);?></h5>
                    </div>
                </div>
            </div>

        </div>
    </div>







    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js" integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf" crossorigin="anonymous"></script>
</body>

</html>




























<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;700&display=swap" rel="stylesheet">
    <title>Document</title>
</head>

<body>

</body>