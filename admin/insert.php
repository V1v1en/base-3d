<?php

require 'database.php';

$name = $description = $category = $image = $nameError = $descriptionError = $categoryError = $imageError = "";

if (!empty($_POST)) {
    $name = verifInput($_POST['name']);
    $description = verifInput($_POST['description']);
    $category = verifInput($_POST['category']);

    // recupération de l'input de type file avec la superglobale FILE (un array dan un array)pour l'image
    // basename retourne le nom du fichier image

    $image = verifInput($_FILES['image']['name']);
    $imagePath = '../images/' . basename($image);
    $imageExtension = pathinfo($imagePath, PATHINFO_EXTENSION);

    $formSuccess = true;
    $uploadSuccess = false;

    // Verification du bon remplissage des champs 

    if (empty($name)) {
        $nameError = 'Chaque objet doit avoir un nom';
        $formSuccess = false;
    }

    if (empty($description)) {
        $descriptionError = 'Chaque objet doit avoir une courte description';
        $formSuccess = false;
    }

    // Facultatif une catégorie est forcement définie par defaut (la premiere de la liste déroulate)

    if (empty($category)) {
        $categoryError = 'Chaque objet doit faire parti d\'une catégorie';
        $formSuccess = false;
    }

    // Facultatif


    if (empty($image)) {
        $imageError = 'Chaque objet doit avoir une image';
        $formSuccess = false;
    } else {

        $uploadSuccess = true;

        if ($imageExtension != "jpg" && $imageExtension != "png" && $imageExtension != "jpeg" && $imageExtension != "gif" && $imageExtension != "jfif") {
            $imageError = "Les fichiers autorises sont: .jpg, .jpeg, .png, .gif, .jfif";
            $uploadSuccess = false;
        }
        if (file_exists($imagePath)) {
            $imageError = "Le fichier existe deja";
            $uploadSuccess = false;
        
        }
        if ($_FILES["image"]["size"] > 500000) {
            $imageError = "Le fichier ne doit pas depasser les 500KB";
            $uploadSuccess = false;
        }
        if ($uploadSuccess) {
            if (!move_uploaded_file($_FILES["image"]["tmp_name"], $imagePath)) {
                $imageError = "Il y a eu une erreur lors de l'upload";
                $uploadSuccess = false;
            }
        }
    }
    if ($formSuccess && $uploadSuccess) {
        $db = Database::connect();
        $statement = $db->prepare("INSERT INTO objects (name,description,category,image) values(?, ?, ?, ?)");
        $statement->execute(array($name, $description, $category, $image));
        Database::disconnect();
        header("Location: index.php");
    }
}


function verifInput($verif)
{
    $verif = trim($verif);                  //supprime les espaces
    $verif = stripslashes($verif);          //supprime les antislashes
    $verif = htmlspecialchars($verif);      //supprime les charactères spéciaux

    return $verif;
}

?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;700&display=swap" rel="stylesheet">
</head>

<body>

    <header>
        <h1 class="text-logo">
            <i class="fas fa-cubes"></i> 3D base <i class="fas fa-cubes"></i>
        </h1>
    </header>

    <div class="container">
        <div class="row creation">
            <div class="col-md-12 dataset">
                <h4 class="minititle">
                    <i class="far fa-plus-square"></i> Ajouter un objet </i>
                </h4>
                <form class="form" method="post" action="insert.php" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="name">Nom de l'objet : </label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Nom" value="<?php printf($name); ?>">
                        <span class="invalid-feedback"><?php echo $nameError; ?></span>
                    </div>

                    <div class="form-group">
                        <label for="description">Description : </label>
                        <input class="form-control" type="text" name="description" class="form-control" placeholder="Description" value="<?php printf($description); ?>">
                        <span class="invalid-feedback"><?php echo $descriptionError; ?></span>
                    </div>

                    <div class="form-group">
                        <label for="category">Catégorie : </label>
                        <select class="form-control" id="category" name="category">

                            <?php
                            $db = Database::connect();
                            foreach ($db->query('SELECT * FROM categories') as $row) {
                                printf("<option value=" . $row['id'] . ">" . $row['name'] . "</option>"); 
                            }
                            Database::disconnect();
                            ?>

                        </select>
                        <span class="invalid-feedback"><?php echo $categoryError; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="name">Ajouter une image : </label>
                        <input type="file" id="image" name="image">
                        <span class="invalid-feedback"><?php echo $imageError; ?></span>
                    </div>

                    <div class="form-action btninsert">
                        <a class="btn btn-dark" href="index.php"><i class="fas fa-arrow-left"></i></a>
                        <button type="submit" class="btn btn-dark"></i> ENVOYER</button>

                    </div>

                </form>
            </div>

        </div>



        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js" integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf" crossorigin="anonymous"></script>
</body>

</html>